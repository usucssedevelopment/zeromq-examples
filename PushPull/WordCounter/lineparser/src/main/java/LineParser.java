import org.zeromq.ZMQ;

import java.nio.ByteBuffer;

public class LineParser {

    public static void main (String[] args) throws Exception {
        ZMQ.Context context = ZMQ.context(1);

        ConsoleUserInterface ui = new ConsoleUserInterface();
        String receiverHost = ui.getUserInput(args, 0, "Enter reader host", "localhost");
        String aggregatorHost = ui.getUserInput(args, 1, "Enter aggregator host", "localhost");

        ZMQ.Socket receiver = context.socket(ZMQ.PULL);
        receiver.connect(String.format("tcp://%s:6001", receiverHost));

        ZMQ.Socket sender = context.socket(ZMQ.PUSH);
        sender.connect(String.format("tcp://%s:6002", aggregatorHost));

        while(!Thread.currentThread ().isInterrupted ()) {

            String line = receiver.recvStr();
            if (line==null && line.isEmpty()) continue;

            String[] words = line.split(" ");
            int wordCount = words.length;
            Thread.sleep(wordCount*2);             // Simulate some other work being done for each word
            System.out.println(String.format("Received a line and counted %d words", wordCount));

            byte[] bytes = convertIntToBytes(wordCount);
            sender.send(bytes);
            Thread.yield();
        }

        receiver.close();
        context.close();
    }

    private static byte[] convertIntToBytes(int value) {
        return ByteBuffer.allocate(4).putInt(value).array();
    }
}
