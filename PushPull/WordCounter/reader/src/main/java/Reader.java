import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import java.io.BufferedReader;
import java.io.FileReader;

public class Reader {
    public static void main (String[] args) throws Exception {
        Context context = ZMQ.context(1);

        Socket sender = context.socket(ZMQ.PUSH);
        sender.bind("tcp://*:6001");

        ConsoleUserInterface ui = new ConsoleUserInterface();
        ui.getContinuationResponse("Type START or S when other processes are ready: ", new String[] { "START", "S"});

        System.out.println("Begin sending tasks to workers\n");

        FileReader fileReader = new FileReader("data/test.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        int lineCount=0;
        String line;
        while((line = bufferedReader.readLine()) != null) {
            lineCount++;
            sender.send(line, 0);
            System.out.println(String.format("Sent out line #%d", lineCount));
            Thread.yield();
        }

        bufferedReader.close();

        sender.close();
        context.close();
    }
}
