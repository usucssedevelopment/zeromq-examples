import org.zeromq.ZMQ;

import java.nio.ByteBuffer;

public class Aggregator {
    public static void main (String[] args) throws Exception {
        ZMQ.Context context = ZMQ.context(1);

        ZMQ.Socket receiver = context.socket(ZMQ.PULL);
        receiver.bind("tcp://*:6002");

        int count = 0;
        while(!Thread.currentThread ().isInterrupted ()) {
            byte[] bytes = receiver.recv();
            if (bytes!=null && bytes.length==4)
                count += convertBytesToInt(bytes);

            System.out.println(String.format("Current total words=%s", count));
            Thread.yield();
        }


        receiver.close();
        context.close();
    }

    private static int convertBytesToInt(byte[] bytes) {
        ByteBuffer wrapped = ByteBuffer.wrap(bytes);
        return wrapped.getInt();
    }
}
