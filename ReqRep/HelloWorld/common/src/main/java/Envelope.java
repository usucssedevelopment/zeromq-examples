import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Envelope {
    private static Logger log = LogManager.getFormatterLogger(Envelope.class.getName());

    protected Message message;

    public Envelope() {}

    public Envelope(Message message) {
        this.message = message;
    }

    public Envelope(byte[] data) {
        Unpackage(data);
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public byte[] getPackagedMessage()
    {
        if (message==null) return null;

        byte[] result = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(message);
            out.flush();
            result = bos.toByteArray();
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (out !=null ) out.close();
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return result;
    }

    public void Unpackage(byte[] bytes)
    {
        message = null;
        if (bytes == null) return;

        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(bis);
            message = (Message) in.readObject();
        }
        catch (IOException | ClassNotFoundException e)
        {
            log.error(e.getMessage());
        }
        finally {
            try {
                if (in != null) in.close();
                bis.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

}
