import java.io.Serializable;

public class HelloRequest extends Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private String firstName;
    private String lastName;
    private PossibleLanguages preferredLanguage;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public PossibleLanguages getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(PossibleLanguages preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public HelloRequest(String firstName, String lastName, PossibleLanguages preferredLanguage) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.preferredLanguage = preferredLanguage;
    }

}
