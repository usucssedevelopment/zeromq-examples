import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Server implements Runnable {

    private Thread thread;
    private boolean keepGoing;
    private ArrayList<String> clientNames = new ArrayList<>();

    private ZContext context;
    private ZMQ.Socket socket;

    Server(ZContext context) {
        this.context = context;
    }

    public void start() {
        if (keepGoing) return;

        keepGoing = true;
        thread = new Thread(this, "HelloWorldServer");
        thread.start();
    }

    public void stop() throws InterruptedException {
        if (!keepGoing) return;

        keepGoing = false;
        while (socket!=null)
            Thread.sleep(10);

        thread = null;
    }

    @Override
    public void run() {
        initialize();

        while (keepGoing && !Thread.currentThread().isInterrupted())
        {
            // Get a request
            String clientName = socket.recvStr(0);
            if (clientName == null) {
                continue;
            }

            // Do some work
            System.out.println(String.format("Received Hello from %s", clientName));
            addToClientNames(clientName);

            // Send reply back to client
            String reply = String.format("Hello, %s -- I'm the World server for %s", clientName, getAllNames());
            socket.send(reply, 0);
        }

        context.destroySocket(socket);
        socket = null;
    }

    private void initialize() {
        socket = context.createSocket(ZMQ.REP);
        socket.setReceiveTimeOut(500);
        socket.setLinger(0);
        socket.bind("tcp://*:26001");
    }

    private void addToClientNames(String name) {
        if (!clientNames.contains(name))
            clientNames.add(name);
    }

    private String getAllNames() {
        return String.join(", ", clientNames);
    }

}
