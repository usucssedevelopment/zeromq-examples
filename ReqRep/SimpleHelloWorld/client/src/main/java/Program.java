import org.zeromq.ZContext;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws Exception {

        ZContext zmqContext = new ZContext(1);

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter server host: ");
        String serverHost = scanner.next();

        System.out.print("Enter your name: ");
        String username = scanner.next();

        Client client = new Client(zmqContext, serverHost, username);
        client.start();

        System.out.println("Type ENTER at anytime to exit");
        System.in.read();

        client.stop();
        zmqContext.close();
    }
}
