import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMQ.Poller;

import java.util.ArrayList;

public class ProxyWorker implements Runnable {

    private Thread thread;
    private boolean keepGoing;
    private ArrayList<String> clientNames = new ArrayList<>();

    private ZContext context;
    private Socket dealer;
    private Socket router;

    ProxyWorker(ZContext context) {
        this.context = context;
    }

    public void start() {
        if (keepGoing) return;

        System.out.println("Starting proxy");
        keepGoing = true;
        thread = new Thread(this, "HelloWorldProxy");
        thread.start();
    }

    public void stop() throws InterruptedException {
        if (!keepGoing) return;

        System.out.println("Stopping proxy");
        keepGoing = false;
        Thread.yield();

        while (dealer!=null || router!=null)
            Thread.sleep(10);

        thread = null;
    }

    @Override
    public void run() {
        initialize();

        //  Initialize poll set
        Poller items = new Poller (2);
        items.register(router, Poller.POLLIN);
        items.register(dealer, Poller.POLLIN);

        while (keepGoing && !Thread.currentThread().isInterrupted())
        {
            items.poll(500);

            if (items.pollin(0))
                receiveAndSend(router, dealer);

            if (items.pollin(1))
                receiveAndSend(dealer, router);
        }

        context.destroySocket(dealer);
        context.destroySocket(router);
        dealer = null;
        router = null;
    }

    private void initialize() {

        router = context.createSocket(ZMQ.ROUTER);
        router.setLinger(0);
        router.bind("tcp://*:26001");

        dealer = context.createSocket(ZMQ.DEALER);
        dealer.setLinger(0);
        dealer.bind("tcp://*:26002");
    }

    private void receiveAndSend(Socket fromSocket, Socket toSocket) {

        System.out.println(String.format("In receiveAndSend with fromSocket=%s and toSocket=%s", fromSocket.toString(), toSocket.toString()));
        byte[] message;
        boolean more = true;

        while (more) {
            message = fromSocket.recv(0);
            more = fromSocket.hasReceiveMore();
            System.out.println(String.format("Got a frame of length=%d with more=%s", message.length, more?"true":"false"));
            toSocket.send(message,  more ? ZMQ.SNDMORE : 0);
        }

    }


}
