import java.io.Serializable;
import java.util.ArrayList;

public class HelloReply extends Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private String greeting;
    private ArrayList<String> clientNames;

    public HelloReply(String greeting, ArrayList<String> clientNames) {
        this.greeting = greeting;
        this.clientNames = clientNames;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public ArrayList<String> getClientNames() {
        return clientNames;
    }

    public void setClientNames(ArrayList<String> clientNames) {
        this.clientNames = clientNames;
    }

}
