import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ServerWorker implements Runnable {

    private static Map<PossibleLanguages, String> greetings;
    static
    {
        greetings = new HashMap<>();
        greetings.put(PossibleLanguages.English, "Hello");
        greetings.put(PossibleLanguages.German, "Hallo");
        greetings.put(PossibleLanguages.Spanish, "Hola");
    }

    private Thread thread;
    private boolean keepGoing;
    private ArrayList<String> clientNames = new ArrayList<>();
    private String proxyHost;
    private String serverName;

    private ZContext context;
    private ZMQ.Socket socket;

    ServerWorker(ZContext context, String proxyHost, String serverName) {
        this.context = context;
        this.proxyHost = proxyHost;
        this.serverName = serverName;
    }

    public void start() {
        if (keepGoing) return;

        System.out.println(String.format("Starting server %s with a connection to proxy at %s", serverName, proxyHost));
        keepGoing = true;
        thread = new Thread(this, String.format("ServerWorker-%s",serverName));
        thread.start();
    }

    public void stop() throws InterruptedException {
        if (!keepGoing) return;

        System.out.println("Stopping server");
        keepGoing = false;
        Thread.yield();
        while (socket!=null)
            Thread.sleep(10);

        thread = null;
    }

    @Override
    public void run() {
        initialize();

        while (keepGoing && !Thread.currentThread().isInterrupted())
        {
            // Get a request
            byte[] requestBytes = socket.recv(0);
            if (requestBytes == null || requestBytes.length <= 0) {
                // System.out.println("No request received");
                continue;
            }

            // Do some work
            Envelope requestEnv = new Envelope(requestBytes);
            if (requestEnv.getMessage()!=null && requestEnv.getMessage().getClass()==HelloRequest.class) {
                HelloRequest request = (HelloRequest) requestEnv.getMessage();
                String clientName = String.format("%s %s", request.getFirstName(), request.getLastName());
                System.out.println(String.format("Received Hello from %s", clientName));
                addToClientNames(clientName);

                String greeting = computeGreeting(request.getPreferredLanguage());

                // Send reply back to client
                HelloReply reply = new HelloReply(greeting, clientNames);
                Envelope replyEnv = new Envelope(reply);
                socket.send(replyEnv.getPackagedMessage(), 0);
            }
        }

        context.destroySocket(socket);
        socket = null;
    }

    private void initialize() {

        socket = context.createSocket(ZMQ.REP);
        socket.setReceiveTimeOut(500);
        socket.setLinger(0);
        socket.connect(String.format("tcp://%s:26002", proxyHost));
    }

    private void addToClientNames(String name) {
        if (!clientNames.contains(name))
            clientNames.add(name);
    }

    private String computeGreeting(PossibleLanguages language) {
        String greeting = "Hello";
        if (greetings.containsKey(language))
            greeting = greetings.get(language);

        greeting += " from server " + serverName;
        return greeting;
    }


}
