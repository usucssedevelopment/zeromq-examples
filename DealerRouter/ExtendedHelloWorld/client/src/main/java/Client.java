import org.zeromq.ZContext;

import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws Exception {

        ZContext zmqContext = new ZContext(1);

        ConsoleUserInterface ui = new ConsoleUserInterface();

        String proxyHost = ui.getUserInput(args, 0, "Enter proxy host", "localhost");
        String firstName = ui.getUserInput(args, 1, "Enter your first name", "John");
        String lastName = ui.getUserInput(args, 2, "Enter your last name", "Jones");
        String languageChoice = ui.getUserInput(args, 3, "Enter preferred language - English, German, Spanish", "English");
        PossibleLanguages preferredLanguage = PossibleLanguages.valueOf(languageChoice);

        ClientWorker clientWorker = new ClientWorker(zmqContext, proxyHost, firstName, lastName, preferredLanguage);
        clientWorker.start();

        ui.getContinuationResponse("Type EXIT or X at anytime to exit: ", new String[] {"EXIT", "X"});

        clientWorker.stop();
        zmqContext.close();
    }

}
