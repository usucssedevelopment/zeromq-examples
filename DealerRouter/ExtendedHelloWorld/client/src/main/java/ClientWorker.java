import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.Random;

public class ClientWorker implements Runnable {

    private Thread thread;
    private boolean keepGoing;

    private String proxyHost;
    private String firstName;
    private String lastName;
    private PossibleLanguages preferredLanguage;
    private ZContext context;
    private ZMQ.Socket socket;
    private Random random = new Random();

    ClientWorker(ZContext context, String proxyHost, String firstName, String lastName, PossibleLanguages preferredLanguage) {
        this.context = context;
        this.proxyHost = proxyHost;
        this.firstName = firstName;
        this.lastName = lastName;
        this.preferredLanguage = preferredLanguage;
    }

    public void start() {
        if (keepGoing) return;

        System.out.println(String.format("Starting client for %s %s with a connection to proxy at %s", firstName, lastName, proxyHost));

        keepGoing = true;
        thread = new Thread(this, String.format("Client-%s%s", firstName,lastName));
        thread.start();
    }

    public void stop() throws InterruptedException {
        if (!keepGoing) return;

        System.out.println("Stopping client");
        keepGoing = false;
        Thread.yield();
        while (thread!=null)
            Thread.sleep(10);
    }

    @Override
    public void run() {
        initialize();

        while (keepGoing && !Thread.currentThread().isInterrupted())
        {
            if (random.nextDouble() < 0.8) continue;

            // Make a request
            HelloRequest request = new HelloRequest(firstName, lastName, preferredLanguage);
            Envelope requstEnv = new Envelope(request);
            socket.send(requstEnv.getPackagedMessage());

            // Wait for a reply
            byte[] replyBytes = socket.recv(0);
            if (replyBytes==null || replyBytes.length==0)
            {
                System.out.println("No response from server -- reconnect and try again");
                initialize();
                continue;
            }

            // Display reply
            Envelope replyEnv = new Envelope(replyBytes);
            if (replyEnv.getMessage()!=null && replyEnv.getMessage().getClass() == HelloReply.class) {
                HelloReply reply = (HelloReply) replyEnv.getMessage();
                System.out.println(reply.getGreeting());
                System.out.print("Current ClientWorker Names: ");
                String allNames = String.join(", ", reply.getClientNames());
                System.out.println(allNames);
            }

            // Take a break
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        context.destroySocket(socket);
        socket = null;
        thread = null;

    }

    private void initialize() {
        if (socket!=null)
            context.destroySocket(socket);

        socket = context.createSocket(ZMQ.REQ);
        socket.setReceiveTimeOut(1000);
        socket.setLinger(0);
        String endPoint = String.format("tcp://%s:26001", proxyHost);
        socket.connect(endPoint);
    }

}
